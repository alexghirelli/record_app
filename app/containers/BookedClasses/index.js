/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { API } from '../App/constants';
import Cookies from '../../utils/cookies';
import RecordFactory from '../../utils/factory';

let factory = new RecordFactory();

// Cmps

import StatusBar from '../../components/StatusBar';
import TopBar from '../../components/TopBar';
import ClassCard from '../../components/ClassCard';

let token = Cookies.getToken('accesstoken');
var urlParams = new URLSearchParams(window.location.search);
var pDate = urlParams.get('date');

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      date: new Date(),
      isLoading: false,
      error: null,
    };
  }

  setClassCardDate() {
    if (pDate) {
      return pDate;
    } else {
      return factory.formatDate(this.state.date);
    }
  }

  render() {
    const { data, isLoading, error } = this.state;

    if (error) {
      return <p>{error.message}</p>;
    }

    if (isLoading) {
      return <p>Loading ...</p>;
    }

    var dataClasses;

    if (data.length > 0) {
    dataClasses = data.map(gymClass =>
      <ClassCard background="gymClass" unenroll="yes" cid={gymClass.cid} classtype={gymClass.type} date={gymClass.class_date} classdate={gymClass.class_date} guid={gymClass.guid} session={gymClass.session} title={gymClass.title} trainer={ gymClass.trainer } from={ gymClass.from } to={ gymClass.to } seats={ gymClass.av_slots } caution={gymClass.caution_time}></ClassCard>
    );
    } else {
      dataClasses = <p style={{textAlign: 'center',padding: '25px'}}>{'Non sono presenti classi oggi. Seleziona un giorno diverso.'}</p>;
    }

    return (
      <div className="wrapper">
        <TopBar title="Classi prenotate"></TopBar>
        <StatusBar></StatusBar>
        <section className="classesList booked">
          {dataClasses}
        </section>
      </div>
    );
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    
    if (token) {
      var today;

      if (this.props.match.params.day) {
        today = this.props.match.params.day;
      } else {
        today = this.state.date.getDay();
      }

      axios.get(
        API + 'classes/booked',
        {
          headers: {
            'x-access-token' : token
          }
        }
      )
      .then((response) => {
          this.setState({
            data: response.data,
            isLoading: false
          })
        },
        (error) => {
          this.setState({
            error: error.response.status,
            isLoading: false
          })
        }
      );
    } else {
      document.location.href = '/';
    }
  }
}