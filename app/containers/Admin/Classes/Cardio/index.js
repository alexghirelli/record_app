/*
 * Admin/Classes
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table, Container, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import fetch from 'cross-fetch';

import { API } from '../../../App/constants';
import Cookies from '../../../../utils/cookies';

import RecordFactory from '../../../../utils/factory';
import TopBar from '../../../../components/TopBar';

let factory = new RecordFactory();

// Styles
import '../AdminClasses.scss';

// Cmps

let token = Cookies.getToken('accesstoken');
var urlParams = new URLSearchParams(window.location.search);
var pDate = urlParams.get('date');
var sessionID = urlParams.get('session');

/* eslint-disable react/prefer-stateless-function */
export default class AdminClasses extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            classes: [],
            users: [],
            user: {},
            date: new Date(),
            isLoading: false,
            error: null,
            modalNew: false
        };
    }

    closeModal(tabId) {
        this.setState({
            [tabId]: false
        });
    }
    showModal(modal) {
        this.setState({
            [modal]: true
        });
    }

    removeUser(classid, uid, session, caution, date, name, surname, classtitle) {
        var confirm = window.confirm("Sei sicuro di voler rimuovere " + name + " " + surname + " dalla classe di: " + classtitle + "?");

        if (confirm == true) {
            fetch(API + 'admin/classes/user/remove', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                },
                body: JSON.stringify({
                    classid: classid,
                    sessionid: session,
                    userid: uid,
                    classdate: date,
                    caution: caution
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if (!responseJson.enrolled && responseJson.message == 'user_unenrolled') {
                    alert('Utente cancellato con successo!');
                    document.location.reload();
                }
                this.setState({
                    modal: false
                })
            })
            .catch((error) => {
                console.log(error);
            });
        }
    }

    registerNewUser() {
        var selectClass = document.getElementById('selectClass');
        var selectUser = document.getElementById('selectUser');

        var cid = selectClass.options[selectClass.selectedIndex].attributes.class.value;
        var sid = selectClass.options[selectClass.selectedIndex].attributes.session.value;
        var date = selectClass.options[selectClass.selectedIndex].attributes.date.value;

        var uid = selectUser.options[selectUser.selectedIndex].value;

        var confirm = window.confirm("Vuoi davvero aggiungere questo utente alla classe?");

        if (confirm == true) {
            fetch(API + 'classes/enroll', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                },
                body: JSON.stringify({
                    classid: cid,
                    sessionid: sid,
                    userid: uid,
                    classdate: date
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if (!responseJson.enrolled && responseJson.message == 'account_freezed') {
                    alert("L'utente ha terminato il numero di lezioni a sua disposizione, pertanto è stato congelato.");
                    document.location.reload();
                }
        
                if (responseJson.enrolled) {
                    alert("L'utente è stato registrato alla classe con successo!");
                    document.location.reload();
                }
    
              this.setState({
                  modal: false
              })
            })
            .catch((error) => {
                console.log(error);
            });
        }
    }

    render() {
        var dataParticipant, _classes, _users, counter = 0, tableDom;
        const { data, classes, users, isLoading, error } = this.state;

        if (data.length > 0) {
            dataParticipant = data.map((participant, i, array) => {
                
                if (i > 2) {
                    if (array[i-1].session != participant.session) {
                        counter = 0
                    }
                }
                
                counter++;

                return (
                    <tr className={participant.type.toLowerCase() + '-' + participant.session}>
                        <td>{counter}</td>
                        <td>{participant.name}</td>
                        <td>{participant.surname}</td>
                        <td><a href={`tel:${participant.phone}`}>{participant.phone}</a></td>
                        <td>{participant.enrollment_date}</td>
                        {this.state.user.permission == 'admin' && (
                            <td><Button color="danger" onClick={(event) => {this.removeUser(participant.classid, participant.uid, participant.session, participant.caution_time, participant.class_date, participant.name, participant.surname, participant.title)}} >Rimuovi utente</Button></td>
                        )}
                    </tr>
                )
            });
        }

        if (classes.length > 0) {
            if (!pDate) {
                pDate = factory.formatDate(new Date());
            }

            _classes = classes.map(_class => 
                <option session={_class.session} class={_class.id} date={pDate}>{_class.title} delle {_class.from}</option>
            );
        }

        if (users.length > 0) {
            _users = users.map(_user => 
                <option value={_user.id}>{_user.name} {_user.surname} | Da poter prenotare: {_user.remaining_lessons}</option>
            );
        }

        tableDom = <Col xs="12">    
            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Cognome</th>
                    <th>Telefono</th>
                    <th>Data iscrizione</th>
                    {this.state.user.permission == 'admin' && sessionID && (
                        <th><Button color="primary" onClick={this.showModal.bind(this, 'modalNew')}>Aggiungi utente</Button></th>
                    )}
                </tr>
                </thead>
                <tbody>
                    {dataParticipant}
                </tbody>
            </Table>

            {/* NEW USER */}
            <Modal className="newUserModal" isOpen={this.state.modalNew} toggle={this.closeModal.bind(this, 'modalNew')} className={this.props.className}>
                <ModalHeader>
                    Iscrizione utente ad una classe
                </ModalHeader>
                <ModalBody>
                    <form className="editUser">
                        <Row>
                            <select id={'selectClass'}>
                                <option value="">Seleziona una classe</option>
                                {_classes}
                            </select>
                            <select id={'selectUser'}>
                                <option value="">Seleziona un utente</option>
                                {_users}
                            </select>
                        </Row>
                    </form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.registerNewUser}>Salva</Button>
                    <Button color="secondary" onClick={this.closeModal.bind(this, 'modalNew')}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </Col>

        if (this.state.data.length == 0) {
            var emptyMessage = <div className="noticeEmpty">
                <p>Seleziona un orario per vedere gli iscritti di quella sessione!</p>
            </div>
        }

        return (
            <div className="adminClasses">
                <Container fluid="true">
                    <TopBar admin="true" calendar="true" namespace="classes" trigger="cardio_fitness" classes={this.state.classes}>Allenamenti <b>Palestra Pesi e Cardio</b></TopBar>
                    <div className="bookedClasses">
                        <Row>
                            {tableDom}
                            {emptyMessage}
                        </Row>
                    </div>
                </Container>
            </div>
        )
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        if (token) {
            if (!pDate) {
                pDate = factory.formatDate(new Date())
            }

            axios.get(
                API + 'users/me',
                {
                    headers: {
                    'x-access-token' : token
                    }
                }
                )
                .then((response) => {
                    this.setState({
                        user: response.data[0],
                        isLoading: false
                    })
                },
                (error) => {
                    this.setState({
                    error: error.response.status,
                    isLoading: false
                    })
                }
            );
    
            axios.get(
            API + 'classes/cardio_fitness/date/' + pDate + '/' + sessionID,
            {
                headers: {
                'x-access-token' : token
                }
            }
            )
            .then((response) => {
                this.setState({
                    data: response.data,
                    isLoading: false
                })
            },
            (error) => {
                this.setState({
                error: error.response.status,
                isLoading: false
                })
            }
            );
    
            this.getClasses();
            this.getUsers();  
        } else {
            document.location.href = '/';
        }
    }

    getClasses() {
        let today;
        
        if (pDate) {
            today = new Date(pDate).getDay();
        } else {
            today = this.state.date.getDay();
        }
    
        axios.get(
            API + 'classes?d=' + today + '&cat=cardio_fitness',
            {
                headers: {
                'x-access-token' : token
                }
            }
            )
            .then((response) => {
                this.setState({
                    classes: response.data,
                    isLoading: false
                })
            },
            (error) => {
                this.setState({
                    error: error.response.status
                })
            }
        );
    }

    getUsers() {
        axios.get(
            API + 'users?type=cardio_fitness,body_mind;cardio_fitness',
            {
                headers: {
                'x-access-token' : token
                }
            }
            )
            .then((response) => {
                this.setState({
                    users: response.data,
                })
            },
            (error) => {
                this.setState({
                    error: error.response.status,
                    isLoading: false
                })
            }
        );
    }
}