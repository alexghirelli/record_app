/*
 * Admin/Users
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table, Container, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import fetch from 'cross-fetch';

import { API } from '../../../App/constants';
import Cookies from '../../../../utils/cookies';

import RecordInput from '../../../../components/Input';
import RecordButton from '../../../../components/Button';
import TopBar from '../../../../components/TopBar';

import RecordFactory from '../../../../utils/factory';
import '../Users.scss';

let factory = new RecordFactory();
let token = Cookies.getToken('accesstoken');

/* eslint-disable react/prefer-stateless-function */
export default class CardioBodyMindUsers extends React.PureComponent {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.autocompleteForm = this.autocompleteForm.bind(this);
        this.state = {
            data: [],
            isLoading: false,
            modal: false,
            user: {},
            modalNew: false
        };
        //this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle(uid, email, phone, freezed, subscription, isactive, remaining_lessons, fold, subscribed_until) {
        this.setState({
            modal: !this.state.modal,
            userData: {
                uid: uid,
                email: email,
                phone: phone,
                freezed: freezed,
                subscription: subscription,
                isactive: isactive,
                remaining_lessons: remaining_lessons,
                fold: fold,
                subscribed_until: subscribed_until
            }
        });
    }

    closeModal(tabId) {
        this.setState({
            [tabId]: false
        });
    }
    showModal(modal) {
        this.setState({
            [modal]: true
        });
    }

    selectElement(id, valueToSelect) {    
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }

    autocompleteForm() {
        let userData = this.state.userData;
        document.getElementById('email').value = userData.email;
        document.getElementById('phone').value = userData.phone;
        this.selectElement('freezed', userData.freezed);
        this.selectElement('subscription', userData.subscription);
        this.selectElement('isactive', userData.isactive);
        document.getElementById('uid').value = userData.uid;
        document.getElementById('fold').value = userData.fold;

        if (userData.subscribed_until) {
            document.getElementById('subscribed_until').value = userData.subscribed_until;
        }
    }

    updateUser() {
        let email = document.getElementById('email').value;
        let phone = document.getElementById('phone').value;

        let freezed = document.getElementById('freezed');
        let freezedVal = freezed.options[freezed.selectedIndex].value;
        
        let subscription = document.getElementById('subscription');
        let subscriptionVal = subscription.options[subscription.selectedIndex].value

        let subscriptionType = document.getElementById('subscription_type');
        let subscriptionTypeVal = subscriptionType.options[subscriptionType.selectedIndex].value

        let subscribedUntil = document.getElementById('subscribed_until').value

        let isactive = document.getElementById('isactive');
        let isactiveVal = isactive.options[isactive.selectedIndex].value;
        let newlessons = document.getElementById('newlessons').value;

        let counter = document.getElementById('fold').value;

        let uid = document.getElementById('uid').value;

        fetch(API + 'user/change/' + uid, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify({
                uid: uid,
                email: email,
                phone: phone,
                freezed: freezedVal,
                subscription: subscriptionVal,
                subscription_type: subscriptionTypeVal,
                subscribeduntil: subscribedUntil,
                isactive: isactiveVal,
                new_lessons: newlessons,
                counter: counter
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.message == 'subscription_changed') {
            alert("L'utente è stato aggiornato con successo");
            document.location.reload();
          }

          this.setState({
              modal: false
          })
        })
        .catch((error) => {
            console.log(error);
        });
    }

    registerNewUser() {
        let name = document.getElementById('newname').value;
        let surname = document.getElementById('newsurname').value;
        let email = document.getElementById('newemail').value;
        let phone = document.getElementById('newphone').value;
        let password = document.getElementById('newpassword').value;
        
        let subscription = document.getElementById('newsubscription');
        let subscriptionVal = subscription.options[subscription.selectedIndex].value;

        let subscriptionType = document.getElementById('new_subscription_type');
        let subscriptionTypeVal = subscriptionType.options[subscriptionType.selectedIndex].value;

        let subscribedUntil = document.getElementById('subscribed_until').value;

        let isactive = document.getElementById('newisactive');
        let isactiveVal = isactive.options[isactive.selectedIndex].value; 

        let remaining_lessons = document.getElementById('newremaining_lessons').value;

        fetch(API + 'auth/insertuser', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify({
                nome: name,
                cognome: surname,
                email: email,
                phone: phone,
                password: password,
                subscription: subscriptionVal,
                subscription_type: subscriptionTypeVal,
                subscribeduntil: subscribedUntil,
                userstatus: isactiveVal,
                remaining_lessons: remaining_lessons
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.message == 'user_registered') {
            alert("L'utente è stato registrato con successo");
            document.location.reload();
          }

          if (responseJson.message == 'already_registered') {
            alert("L'utente risulta già registrato!");
            document.location.reload();
          }

          this.setState({
              modal: false
          })
        })
        .catch((error) => {
            console.log(error);
        });
    }

    removeUser() {
        let uid = document.getElementById('uid').value;

        var confirm = window.confirm("Sei sicuro di voler rimuovere questo utente?");

        if (confirm == true) {
            fetch(API + 'auth/remove/' + uid, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
              if (responseJson.message == 'user_removed') {
                alert("L'utente è stato rimosso con successo");
                document.location.reload();
              }
    
              if (responseJson.message == 'not_allowed') {
                alert("Non sei abilitato a compiere quest'azione");
                document.location.reload();
              }
    
              this.setState({
                  modal: false
              })
            })
            .catch((error) => {
                console.log(error);
            });
        }
    }
    
    render() {
        var dataUsers;

        if (this.state.data.length > 0) {
            dataUsers = this.state.data.map((user,i) =>
                <tr>
                    <td>{i + 1}</td>
                    <td>{user.name}</td>
                    <td>{user.surname}</td>
                    <td>{new Date(moment(user.subscribed_until).toDate()).toLocaleString()}</td>
                    <td>{user.subscription}</td>
                    <td><span className={user.isactive === 1 && user.freezed === 1 ? 'disactive blink' : user.isactive === 0 ? 'disactive' : 'active'}>&#9673;</span></td>
                    <td>{user.remaining_lessons + user.booked_lessons}</td>
                    <td>{user.done_lessons}</td>
                    <td>{user.booked_lessons}</td>
                    {this.state.user.permission == 'admin' && (
                        <td><Button color="danger" onClick={(event) => { this.toggle(user.id, user.email, user.phone, user.freezed, user.subscription, user.isactive, (user.remaining_lessons + user.booked_lessons), user.fold, user.subscribed_until);}}>Modifica</Button></td>
                    )}
                </tr>
            );
        }
        return (
            <div>
                <TopBar admin="true" calendar="false" namespace="users">
                    Gestione utenti <b>Palestra Pesi e Cardio + Body Mind</b>
                </TopBar>
                <div className="adminUsers"> 
                    <Container fluid="true">
                        <Row>
                            <Col xs="12">
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Cognome</th>
                                            <th>Valido fino</th>
                                            <th>Abbonamento</th>
                                            <th>Stato</th>
                                            <th>Lezioni restanti</th>
                                            <th>Lezioni fatte</th>
                                            <th>Lezioni prenotate</th>
                                            {this.state.user.permission == 'admin' && (
                                                <th><Button color="primary" onClick={this.showModal.bind(this, 'modalNew')}>Nuovo utente</Button></th>
                                            )}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {dataUsers}

                                        <Modal className="editUserModal" isOpen={this.state.modal} onOpened={this.autocompleteForm} toggle={this.toggle} className={this.props.className}>
                                            <ModalHeader>
                                                <Button color="primary" onClick={this.updateUser}>Salva</Button>
                                                <Button color="danger" onClick={this.removeUser}>Rimuovi utente</Button>
                                                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                            </ModalHeader>
                                            <ModalBody>
                                                <form className="editUser">
                                                    <Row>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="email">Email</Label>
                                                                <Input type="email" name="email" id="email" placeholder="Email" />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="subscription">Abbonamento</Label>
                                                                <select id="subscription" name="subscription">
                                                                    <option value="open">Open</option>
                                                                    <option value="atermine">A termine</option>
                                                                </select>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="subscription_type">Tipo Abbonamento</Label>
                                                                <select id="subscription_type" name="subscription_type">
                                                                    <option value="body_mind;cardio_fitness">Palestra pesi e cardio + Body Mind</option>
                                                                    <option value="cardio_fitness">Palestra pesi e cardio</option>
                                                                    <option value="body_mind">Body Mind</option>
                                                                </select>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="freezed">Congelato</Label>
                                                                <select id="freezed" name="freezed">
                                                                    <option value="1">Si</option>
                                                                    <option value="0">No</option>
                                                                </select>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="isactive">Stato</Label>
                                                                <select id="isactive" name="isactive">
                                                                    <option value="1">Attivo</option>
                                                                    <option value="0">Disattivo</option>
                                                                </select>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="subscribed_until">Abbonamento valido fino a</Label>
                                                                <Input type="text" name="subscribed_until" id="subscribed_until" />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="phone">Phone</Label>
                                                                <Input type="text" name="phone" id="phone" placeholder="Telefono" />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="fold">Contatore annullamenti (settalo a 0 quando "scongeli un utente")</Label>
                                                                <Input type="text" name="fold" id="fold" placeholder="Annullamenti" />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="newlessons">Lezioni da accreditare</Label>
                                                                <Input type="number" name="newlessons" id="newlessons" placeholder="Lezioni da accreditare" />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Input type="hidden" name="uid" id="uid" />
                                                </form>
                                            </ModalBody>
                                        </Modal>
                                    </tbody>
                                </Table>

                                {/* NEW USER */}
                                <Modal className="newUserModal" isOpen={this.state.modalNew} toggle={this.closeModal.bind(this, 'modalNew')} className={this.props.className}>
                                    <ModalHeader>
                                        Nuovo utente
                                    </ModalHeader>
                                    <ModalBody>
                                        <form className="editUser">
                                            <Row>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newname">Nome</Label>
                                                        <Input type="text" name="newname" id="newname" placeholder="Nome" required={true} />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="surname">Cognome</Label>
                                                        <Input type="text" name="newsurname" id="newsurname" placeholder="Cognome" required={true} />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newemail">Email</Label>
                                                        <Input type="email" name="newemail" id="newemail" placeholder="Email" required={true} />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newphone">Telefono</Label>
                                                        <Input type="number" name="newphone" id="newphone" placeholder="Telefono" required={true} />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newpassword">Password</Label>
                                                        <Input type="password" name="newpassword" id="newpassword" placeholder="Password" required={true} />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newsubscription">Abbonamento</Label>
                                                        <select id="newsubscription" name="newsubscription" required={true}>
                                                            <option value="open">Open</option>
                                                            <option value="atermine">A termine</option>
                                                        </select>
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="new_subscription_type">Tipo Abbonamento</Label>
                                                        <select id="new_subscription_type" name="new_subscription_type">
                                                            <option value="body_mind;cardio_fitness">Palestra pesi e cardio + Body Mind</option>
                                                            <option value="cardio_fitness">Palestra pesi e cardio</option>
                                                            <option value="body_mind">Body Mind</option>
                                                        </select>
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="subscribed_until">Abbonamento valido fino a</Label>
                                                        <Input type="text" name="subscribed_until" id="subscribed_until" required={true} />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newisactive">Stato</Label>
                                                        <select id="newisactive" name="newisactive">
                                                            <option value="1">Attivo</option>
                                                            <option value="0">Disattivo</option>
                                                        </select>
                                                    </FormGroup>
                                                </Col>
                                                <Col xs="12">
                                                    <FormGroup>
                                                        <Label for="newremaininglessons">Lezioni restanti</Label>
                                                        <Input type="number" name="newremaining_lessons" id="newremaining_lessons" required={true} placeholder="Lezioni restanti" />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Input type="hidden" name="uid" id="uid" />
                                        </form>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="primary" onClick={this.registerNewUser}>Salva</Button>
                                        <Button color="secondary" onClick={this.closeModal.bind(this, 'modalNew')}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        if (token) {
            axios.get(
                API + 'users/me',
                {
                    headers: {
                    'x-access-token' : token
                    }
                }
                )
                .then((response) => {
                    this.setState({
                        user: response.data[0],
                        isLoading: false
                    })
                },
                (error) => {
                    this.setState({
                    error: error.response.status,
                    isLoading: false
                    })
                }
            );

            axios.get(
            API + 'users?type=body_mind;cardio_fitness',
            {
                headers: {
                'x-access-token' : token
                }
            }
            )
            .then((response) => {
                this.setState({
                data: response.data,
                isLoading: false
                })
            },
            (error) => {
                this.setState({
                error: error.response.status,
                isLoading: false
                })
            }
            );
        } else {
            document.location.href = '/';
        }
    }
}