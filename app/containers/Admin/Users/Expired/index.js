/*
 * Admin/Users
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table, Container, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import fetch from 'cross-fetch';

import { API } from '../../../App/constants';
import Cookies from '../../../../utils/cookies';

import RecordInput from '../../../../components/Input';
import RecordButton from '../../../../components/Button';
import TopBar from '../../../../components/TopBar';

import RecordFactory from '../../../../utils/factory';
import '../Users.scss';

let factory = new RecordFactory();
let token = Cookies.getToken('accesstoken');

/* eslint-disable react/prefer-stateless-function */
export default class ExpiredUsers extends React.PureComponent {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.autocompleteForm = this.autocompleteForm.bind(this);
        this.state = {
            data: [],
            isLoading: false,
            modal: false,
            user: {},
            modalNew: false
        };
        //this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle(uid, name, surname, email, phone, subscription, subscribed_until) {
        this.setState({
            modal: !this.state.modal,
            userData: {
                uid: uid,
                name: name,
                surname: surname,
                email: email,
                phone: phone,
                subscription: subscription,
                subscribed_until: subscribed_until
            }
        });
    }

    closeModal(tabId) {
        this.setState({
            [tabId]: false
        });
    }
    showModal(modal) {
        this.setState({
            [modal]: true
        });
    }

    selectElement(id, valueToSelect) {    
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }

    autocompleteForm() {
        console.log(this.state)
        let userData = this.state.userData;

        this.selectElement('subscription', userData.subscription);
        document.getElementById('uid').value = userData.uid;

        if (userData.subscribed_until) {
            document.getElementById('subscribed_until').value = userData.subscribed_until;
        }
    }

    aliveUser() {
        let subscription = document.getElementById('subscription');
        let subscriptionVal = subscription.options[subscription.selectedIndex].value;
        let subscribedUntil = document.getElementById('subscribed_until').value;
        let newlessons = document.getElementById('newlessons').value;

        let uid = document.getElementById('uid').value;

        fetch(API + 'users/alive', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify({
                uid: uid,
                subscription: subscriptionVal,
                subscribed_until: subscribedUntil,
                remaining_lessons: newlessons,
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.message == 'reenabled_subscription') {
            alert("L'utente è stato riattivato con successo");
            document.location.reload();
          }

          this.setState({
              modal: false
          })
        })
        .catch((error) => {
            console.log(error);
        });
    }

    removeUser() {
        let uid = document.getElementById('uid').value;

        var confirm = window.confirm("Sei sicuro di voler rimuovere questo utente?");

        if (confirm == true) {
            fetch(API + 'auth/remove/' + uid, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
              if (responseJson.message == 'user_removed') {
                alert("L'utente è stato rimosso con successo");
                document.location.reload();
              }
    
              if (responseJson.message == 'not_allowed') {
                alert("Non sei abilitato a compiere quest'azione");
                document.location.reload();
              }
    
              this.setState({
                  modal: false
              })
            })
            .catch((error) => {
                console.log(error);
            });
        }
    }
    
    render() {
        var dataUsers;

        if (this.state.data.length > 0) {
            dataUsers = this.state.data.map((user,i) =>
                <tr>
                    <td>{i + 1}</td>
                    <td>{user.name}</td>
                    <td>{user.surname}</td>
                    <td>{user.subscription}</td>
                    <td>{user.remaining_lessons}</td>
                    {this.state.user.permission == 'admin' && (
                        <td><Button color="danger" onClick={(event) => { this.toggle(user.id, user.name, user.surname, user.email, user.phone, user.subscription, user.subscribed_until);}}>Riattiva</Button></td>
                    )}
                </tr>
            );
        }
        return (
            <div>
                <TopBar admin="true" calendar="false" namespace="users">
                    Gestione utenze scadute
                </TopBar>
                <div className="adminUsers"> 
                    <Container fluid="true">
                        <Row>
                            <Col xs="12">
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Cognome</th>
                                            <th>Abbonamento</th>
                                            <th>Lezioni restanti</th>
                                            <th>Azione </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {dataUsers}

                                        <Modal className="editUserModal" isOpen={this.state.modal} onOpened={this.autocompleteForm} toggle={this.toggle} className={this.props.className}>
                                            <ModalHeader toggle={this.toggle}></ModalHeader>
                                            <ModalBody>
                                                <form className="editUser">
                                                    <Row>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="subscription">Abbonamento</Label>
                                                                <select id="subscription" name="subscription">
                                                                    <option value="open">Open</option>
                                                                    <option value="atermine">A termine</option>
                                                                </select>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="subscribed_until">Abbonamento valido fino a</Label>
                                                                <Input type="text" name="subscribed_until" id="subscribed_until" />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label for="newlessons">Lezioni da accreditare</Label>
                                                                <Input type="number" name="newlessons" id="newlessons" placeholder="Lezioni da accreditare" />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Input type="hidden" name="uid" id="uid" />
                                                </form>
                                            </ModalBody>
                                            <ModalFooter>
                                                <Button color="danger" onClick={this.removeUser} style={{position: 'absolute', left: 15}}>Rimuovi utente</Button>
                                                <Button color="primary" onClick={this.aliveUser}>Salva</Button>
                                                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                            </ModalFooter>
                                        </Modal>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        if (token) {
            axios.get(
                API + 'users/me',
                {
                    headers: {
                    'x-access-token' : token
                    }
                }
                )
                .then((response) => {
                    this.setState({
                        user: response.data[0],
                        isLoading: false
                    })
                },
                (error) => {
                    this.setState({
                    error: error.response.status,
                    isLoading: false
                    })
                }
            );
            
            axios.get(
            API + 'users/expired',
            {
                headers: {
                    'x-access-token' : token
                }
            }
            )
            .then((response) => {
                this.setState({
                data: response.data,
                isLoading: false
                })
            },
            (error) => {
                this.setState({
                error: error.response.status,
                isLoading: false
                })
            }
            );
        } else {
            document.location.href = '/';
        }
    }
}