/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { API } from '../App/constants';
import Cookies from '../../utils/cookies';

// Cmps

import StatusBar from '../../components/StatusBar';
import ClassCard from '../../components/ClassCard';

let token = Cookies.getToken('accesstoken');

/* eslint-disable react/prefer-stateless-function */
export default class User extends React.PureComponent {

  render() {
    return '';
  }
}