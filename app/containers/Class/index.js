/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { API } from '../App/constants';
import Cookies from '../../utils/cookies';

import RecordFactory from '../../utils/factory';

let factory = new RecordFactory();

// Cmps

import TopBar from '../../components/TopBar';
import StatusBar from '../../components/StatusBar';
import { cidr } from 'ip';

import './Class.scss';

let token = Cookies.getToken('accesstoken');
var urlParams = new URLSearchParams(window.location.search);
var pDate = urlParams.get('date');

var urlData = document.location.href.split('/'),
    idClass = urlData[4],
    idSession = urlData[5],
    folded = urlData[6];

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: false,
      error: null,
      canShowModal: false
    };

    this.showCovidModal = this.showCovidModal.bind(this)
  }

  backToClasses() {
    let dayNumber = new Date(pDate).getDay();
    let url = '/classes/next/' + dayNumber + '?date=' + pDate

    return url;
  }

  render() {
    const { available_slots, data, isLoading, error } = this.state;

    if (error) {
      return <p>{error.message}</p>;
    }

    if (isLoading) {
      return <p>Loading ...</p>;
    }

    var enrollButton, unEnrollButton, buttons;

    if (available_slots > 0) {
      if (!this.state.isAlreadyEnrolled) {
        enrollButton = <button className="enroll_button" onClick={this.showCovidModal} cid={data.class} sid={data.session} cdate={pDate}>Iscrivimi</button>;
      } else {
        enrollButton = <button className="enroll_button enrolled" disabled="disabled">Sei già iscritto a questa classe!</button>;
        unEnrollButton = <button className="enroll_button" onClick={this.unEnrollUserToClass} cid={data.class} sid={data.session} cdate={pDate} caution={data.caution_time}>Rimuovi la mia iscrizione!</button>
      }
  
      if (!this.state.folded) {
        buttons = <div>
          <p style={{textAlign:'center'}}>{enrollButton}</p>
          <p style={{textAlign:'center'}}>{unEnrollButton}</p>
        </div> 
      } else {
        buttons = <div style={{textAlign: 'center'}}>
          <button className="enroll_button enrolled" disabled="disabled">Questa classe non è più disponibile!</button>
        </div>
      }
    } else {
      buttons = <div style={{textAlign: 'center'}}>
        <button className="enroll_button enrolled" disabled="disabled">Posti terminati! Riprova più tardi!</button>
      </div>
    }

    return (
      <div>
        <div className="wrapper class">
          <div className={'headerImage ' + data.type }>
            <a className="back" href={this.backToClasses()}> ❮ </a>
            <div className="overlayShadow"></div>

            <div className="titleInfo">
              <h1>{data.title}</h1>
              {data.trainer && (
                <h2>Trainer: {data.trainer}</h2>
              )}
              <span>Posti disponibili: {available_slots}</span>
            </div>
          </div>

          <div className="classBody">
            <p>{data.description}</p>
            
            {buttons}
          </div>
          <StatusBar></StatusBar>
        </div>
        <div className="covidmodal" style={this.state.canShowModal ? {display:'flex'}: {display:'none'}}>
          <div className="text">
            <p>
              PER POTER ACCEDERE AL SISTEMA DI ISCRIZIONE E FREQUENTARE LA PALESTRA IL GIORNO PRESCELTO DICHIARO QUANTO SEGUE: <br /><br />
              1-	di non essere attualmente affetto da COVID-19 e di non essere stato a periodo di quarantena obbligatoria negli ultimi 14 gg;<br />
              2-	di non essere affetto attualmente da patologia febbrile con temperatura pari o superiore a 37,5° C; <br />
              3-	di non accusare al momento, tosse insistente, difficoltà respiratoria, raffreddore, mal di gola, cefalea, forte astenia (stanchezza), diminuzione o perdita di olfatto/gusto, diarrea;<br />
              4-	di non aver avuto contatti con persona affetta da COVID-19 nelle 48 ore precedenti la comparsa dei sintomi; <br />
              5-	di non aver avuto contatti stretti con una persona affetta da COVID- 19 negli ultimi 14 giorni; <br />
              6-	di non recarmi in palestra se presento uno o più dei precedenti casi;<br />
            </p>
          </div>
          <div className="buttons">
            <button className="enroll_button" onClick={this.enrollUserToClass} cid={data.class} sid={data.session} cdate={pDate}>DICHIARO</button>
            <button className="enroll_button negative" onClick={this.logout}>NON DICHIARO</button>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    if (token) {
      this.getClassData();
      this.availableSlots();
      this.isAlreadyEnrolled();
    } else {
      document.location.href = '/';
    }
  }

  getClassData() {
    var that = this;

    axios.get(
      API + 'classes/' + idClass,
      {
        headers: {
          'x-access-token' : token
        }
      }
    )
    .then((response) => {
        that.setState({
          data: response.data[0],
          isLoading: false
        })

        if (that.state.data.enrolled == true) {
          Cookies.setCookie('enrolled', true, 1);
        }
      },
      (error) => {
        that.setState({
          error: error.response.status,
          isLoading: false
        })
      }
    );
  }

  isAlreadyEnrolled(req, res) {
    axios.get(
      API + 'classes/enrolled/' + idSession + '/' + pDate,
      {
        headers: {
          'x-access-token' : token
        }
      }
    )
    .then((response) => {
        this.setState({
          isAlreadyEnrolled: response.data.alreadyEnrolled
        })
      },
      (error) => {
        this.setState({
          error: error.response.status,
          isLoading: false
        })
      }
    );
  }

  availableSlots() {
    var that = this;

    axios.get(
      API + 'classes/booked/' + idSession + '/' + pDate,
      {
        headers: {
          'x-access-token' : token
        }
      }
    )
    .then((response) => {
        let available = response.data.available

        that.setState({
          available_slots: available
        })
      },
      (error) => {
        that.setState({
          error: error.response.status,
          isLoading: false
        })
      }
    );
  }

  hasFolded() {
    var that = this;

    axios.get(
      API + 'classes/folded/' + idSession + '/' + pDate,
      {
        headers: {
          'x-access-token' : token
        }
      }
    )
    .then((response) => {
        that.setState({
          folded: response.data.folded
        })
      },
      (error) => {
        that.setState({
          error: error.response.status,
          isLoading: false
        })
      }
    );
  }

  showCovidModal() {
    this.setState({ canShowModal: true });
  }

  logout() {
    var confirm = window.confirm("Hai negato il consenso alle regole Anti-COVID. Verrai riportato alla home.");

    if (confirm === true) {
      document.location.href = '/';
    } 
  }

  enrollUserToClass(e) {
    let cid = e.target.getAttribute('cid');
    let sid = e.target.getAttribute('sid');
    let cdate = e.target.getAttribute('cdate');

    factory.enrollUserToClass(API, token, cid, sid, cdate, '', Cookies);
  }

  unEnrollUserToClass(e) {
    let cid = e.target.getAttribute('cid');
    let sid = e.target.getAttribute('sid');
    let cdate = e.target.getAttribute('cdate');
    let caution = e.target.getAttribute('caution');

    factory.unEnrollUserToClass(API, token, cid, sid, cdate, caution, Cookies);
  }
}