/*
 * Login Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
    payoff: {
      id: 'app.components.Register.payoff',
      defaultMessage: 'Register new account and enter into Record Body Line world',
    },
    name: {
      id: 'app.components.Register.name',
      defaultMessage: 'Nome',
    },
    surname: {
      id: 'app.components.Register.surname',
      defaultMessage: 'Cognome',
    },
    email: {
        id: 'app.components.Register.email',
        defaultMessage: 'Email',
    },
    phone: {
        id: 'app.components.Register.phone',
        defaultMessage: 'Telefono',
    },
    password: {
        id: 'app.components.Register.password',
        defaultMessage: 'Password',
    },
    register: {
        id: 'app.components.Register.register',
        defaultMessage: 'Registrami!',
    },
    withaccount: {
        id: 'app.components.Register.witchaccount',
        defaultMessage: "Hai già un'account?",
    },
    login: {
        id: 'app.components.Register.login',
        defaultMessage: "Login",
    },
  });