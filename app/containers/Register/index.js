/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { Container, Row, Col, Button, Form, FormGroup, Input } from 'reactstrap';
import { API } from '../App/constants';
import Cookies from '../../utils/cookies';
import fetch from 'cross-fetch';

// Components

import RecordButton from '../../components/Button';
import RecordInput from '../../components/Input';

// Styles
import './Register.scss';

/* eslint-disable react/prefer-stateless-function */
export default class Register extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    let email = this.state.email;
    let password = this.state.password;
    let name = this.state.name;
    let surname = this.state.surname;
    let phone = this.state.phone;
    let subscription = this.state.subscription;
    let subscription_type = this.state.subscription_type;

    fetch(API + 'auth/register', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: email,
          email: email,
          password: password,
          subscription: subscription,
          subscription_type: subscription_type,
          nome: name,
          cognome: surname,
          phone: phone
        })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.message == 'already_registered') {
        alert("La tua registrazione è ancora in fase di approvazione. Al più presto potrai accedere.")
      } else {
        alert("Grazie per esserti registrato. Per poter usare l'app, il tuo account deve essere abilitato da un nostro operatore. Verrai ricontattato appena avverrà l'operazione. Oppure riprova più tardi.");
      }
    })
    .catch((error) => {
        console.log(error);
    });
    
    event.preventDefault();
  }

  getValue = (field) => {
    return this.state[field] || "";
  };

  render() {
    return(
      <div className="register_wrapper">
        <p style={{textAlign: 'center'}}><img className="logo" src={require('../../images/logo_rbl.png')} /></p>
        <div className="overlay"></div>
        <Form onSubmit={this.handleSubmit}>
          <Container>
            <div className="payoff">
              <Row>
                <Col xs="12">
                  <p>Registra un nuovo account <br /> ed entra nel mondo di <br /> Record Body Line </p>
                </Col>
              </Row>
            </div>
            <Row>
              <Col xs="12">
                <RecordInput type="text" placeholder={<FormattedMessage {...messages.name} />} name="name" onChange={this.handleChange}></RecordInput>
              </Col>
              <Col xs="12">
                <RecordInput type="text" placeholder={<FormattedMessage {...messages.surname} />} name="surname" onChange={this.handleChange}></RecordInput>
              </Col>
              <Col xs="12">
                <RecordInput type="email" placeholder={<FormattedMessage {...messages.email} />} name="email" onChange={this.handleChange}></RecordInput>
              </Col>
              <Col xs="12">
                <RecordInput type="phone" placeholder={<FormattedMessage {...messages.phone} />} name="phone" onChange={this.handleChange}></RecordInput>
              </Col>
              <Col xs="12">
                <RecordInput type="password" placeholder={<FormattedMessage {...messages.password} />} name="password" onChange={this.handleChange}></RecordInput>
              </Col>
              <Col xs="12" style={{ position: 'relative' }}>
                <select name="subscription_type" onChange={this.handleChange}>
                  <option>Seleziona abbonamento</option>
                  <option value="body_mind">BodyMind</option>
                  <option value="cardio_fitness">Palestra pesi e cardio</option>
                  <option value="body_mind;cardio_fitness">Palestra pesi e cardio + Body Mind</option>
                </select>
                <input className="selectRequired" value={this.getValue('subscription_type')} required={true}/>
              </Col>
              <Col xs="12" style={{ position: 'relative' }}>
                <select name="subscription" onChange={this.handleChange}>
                  <option>Seleziona tipo abbonamento</option>
                  <option value="open">Open</option>
                  <option value="atermine">A termine</option>
                </select>
                <input className="selectRequired" value={this.getValue('subscription')} required={true} />
              </Col>
              <Col xs="12">
                <RecordButton color="red" type="submit" message={<FormattedMessage {...messages.register} />}></RecordButton>
              </Col>
            </Row>
            <div className="payoff" style={{marginTop: '20px'}}>
              <Row>
                <Col xs="12">
                  <p><FormattedMessage {...messages.withaccount} /> <a href="/"><FormattedMessage {...messages.login} /></a></p>
                </Col>
              </Row>
            </div>
          </Container>
        </Form>
      </div>
    )
  }
}
