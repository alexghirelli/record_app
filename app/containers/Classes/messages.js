/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  desktopnotallowed: {
    id: 'app.components.Classes.desktopnotallowed',
    defaultMessage: 'You can use this applications only on mobile! Visit this page with your phone!',
  },
});
