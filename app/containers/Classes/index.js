/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { API } from '../App/constants';
import Cookies from '../../utils/cookies';
import RecordFactory from '../../utils/factory';

let factory = new RecordFactory();

// Cmps

import StatusBar from '../../components/StatusBar';
import TopBar from '../../components/TopBar';
import ClassCard from '../../components/ClassCard';

let token = Cookies.getToken('accesstoken');
var urlParams = new URLSearchParams(window.location.search);
var pDate = urlParams.get('date');

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      date: new Date(),
      user: {},
      isLoading: false,
      error: null,
    };
  }

  setClassCardDate() {
    if (pDate) {
      return pDate;
    } else {
      return factory.formatDate(this.state.date);
    }
  }

  render() {
    const { data, isLoading, error } = this.state;

    if (error) {
      return <p>{error.message}</p>;
    }

    if (isLoading) {
      return <p>Loading ...</p>;
    }

    var dataClasses;

    if (Object.keys(this.state.user).length === 0) {
      document.location.reload();
    }

    if (data.length > 0) {
    dataClasses = data.map(gymClass => {
      if (gymClass.isactive == 1 && this.state.user.subscription_type.includes(gymClass.category)) {
        return <ClassCard background="gymClass" classtype={gymClass.type} date={this.setClassCardDate()} guid={gymClass.guid} session={gymClass.session} title={gymClass.title} trainer={ gymClass.trainer } from={ gymClass.from } to={ gymClass.to } seats={ gymClass.av_slots }></ClassCard> 
      }
    });
    } else {
      dataClasses = <p style={{textAlign: 'center',padding: '25px'}}>{'Non sono presenti classi oggi. Seleziona un giorno diverso.'}</p>;
    }
    
    return (
      <div className="wrapper">
        <TopBar calendar="true" admin="false" rl={this.state.user.remaining_lessons + this.state.user.booked_lessons} bl={this.state.user.remaining_lessons} dl={this.state.user.done_lessons} su={this.state.user.subscribed_until}></TopBar>
        <StatusBar></StatusBar>
        <section className="classesList">
          {dataClasses}
        </section>
      </div>
    );
  }

  componentWillMount() {
    this.setState({ isLoading: true });
    if (token) {
      var today;

      if (this.props.match.params.day) {
        today = this.props.match.params.day;
      } else {
        today = this.state.date.getDay();
      }

      axios.get(
        API + 'users/me',
        {
            headers: {
            'x-access-token' : token
            }
        }
        )
        .then((response) => {
            this.setState({
                ...this.state.data,
                ...this.state.date,
                user: response.data[0],
                isLoading: false
            })
        },
        (error) => {
            this.setState({
            error: error.response.status,
            isLoading: false
            })
        }
      );

      axios.get(
        API + 'classes?d=' + today,
        {
          headers: {
            'x-access-token' : token
          }
        }
      )
      .then((response) => {
          this.setState({
            ...this.state.user,
            ...this.state.date,
            data: response.data,
            isLoading: false
          })
        },
        (error) => {
          this.setState({
            error: error.response.status,
            isLoading: false
          })
        }
      );
    } else {
      document.location.href = '/';
    }
  }
}