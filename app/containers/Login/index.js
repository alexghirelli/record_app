/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { Container, Row, Col, Button, Form, FormGroup, Input } from 'reactstrap';
import { API } from '../App/constants';
import Cookies from '../../utils/cookies';
import fetch from 'cross-fetch';

// Components

import RecordButton from '../../components/Button';
import RecordInput from '../../components/Input';

// Styles
import './Login.scss';

/* eslint-disable react/prefer-stateless-function */
export default class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  isUserLogged() {
    return Cookies.getToken('accesstoken');
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
  }

  handleSubmit(event) {
    let email = this.state.email;
    let password = this.state.password;

    document.getElementById('login_button').setAttribute('disabled', 'disabled');

    fetch(API + 'auth/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then((response) => response.json())
    .then((responseJson) => {

      if(responseJson.auth) {
        Cookies.setCookie('accesstoken', responseJson.token, 30);
        document.location = '/classes/next';
      } else {
        alert(responseJson.message);
        document.getElementById('login_button').removeAttribute('disabled', 'disabled');
      }

    })
    .catch((error) => {
        console.log(error);
    });
    
    event.preventDefault();
  }

  render() {
    if (this.isUserLogged()) {
      document.location = '/classes/next';
    } else {
      return(
        <div className="login_wrapper">
          <p style={{textAlign: 'center'}}><img class="logo" src={require('./logo_rbl.png')} /></p>
          <Form onSubmit={this.handleSubmit}>
            <Container>
              <Row>
                <Col xs="12">
                  <RecordInput type="text" placeholder={<FormattedMessage {...messages.email} />} name="email" onChange={this.handleChange} required="true"></RecordInput>
                </Col>
                <Col xs="12">
                  <RecordInput type="password" placeholder={<FormattedMessage {...messages.password} />} name="password" onChange={this.handleChange} required="true"></RecordInput>
                </Col>
  
                <Col xs="12">
                  <RecordButton color="red" type="submit" id="login_button" message={<FormattedMessage {...messages.login} />}></RecordButton>
                </Col>
  
                <Col xs="12">
                  <RecordButton color="grey" type="register" id="register_button" message={<FormattedMessage {...messages.register} />}></RecordButton>
                </Col>
              </Row>
            </Container>
          </Form>
        </div>
      )
    }
  }
}
