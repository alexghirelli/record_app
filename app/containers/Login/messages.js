/*
 * Login Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
    default: {
      id: 'app.components.Login.default',
      defaultMessage: 'Clicca qui',
    },
    login: {
      id: 'app.components.Login.login',
      defaultMessage: 'Login',
    },
    register: {
      id: 'app.components.Login.register',
      defaultMessage: 'Registrati',
    },
    email: {
        id: 'app.components.Login.email',
        defaultMessage: 'Email',
    },
    password: {
        id: 'app.components.Login.password',
        defaultMessage: 'Password',
    },
    noaccount: {
        id: 'app.components.Login.noaccount',
        defaultMessage: "Non hai un account?",
    },
    passwordlost: {
        id: 'app.components.Login.passwordlost',
        defaultMessage: "Hai perso la password?",
    },
  });