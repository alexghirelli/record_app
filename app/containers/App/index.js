/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NextClasses from '../Classes/Loadable';
import BookedClasses from '../BookedClasses/Loadable';
import Class from '../Class/Loadable';
import Login from '../Login/Loadable'; 
import Register from '../Register/Loadable';
import NotFoundPage from '../NotFoundPage/Loadable';

// Admin
import AdminUsers from '../Admin/Users/Loadable'; 
import AdminClassC from '../Admin/Classes/Cardio/Loadable';
import AdminClassBM from '../Admin/Classes/Bmind/Loadable';
import ExpiredUsers from '../Admin/Users/Expired/Loadable';
import BodyMindUsers from '../Admin/Users/Bodymind/Loadable';
import CardioUsers from '../Admin/Users/Cardio/Loadable';
import CardioBodyMindUsers from '../Admin/Users/Cardiobodymind/Loadable';


// Styles

import './App.scss';

export default function App() { 
  return (
    <div>
      <Switch>
        {/* CLASSES */}
        <Route exact path="/" component={Login} />
        <Route exact path="/classes/next" component={NextClasses} />
        <Route exact path="/classes/next/:day" component={NextClasses} />
        <Route exact path="/classes/booked" component={BookedClasses} />
        <Route exact path="/classes/:id/:session" component={Class} />

        {/* USER */}
        {/* <Route exact path="/user" component={User} /> */}
        <Route exact path="/register" component={Register} />

        {/* ADMIN */}
        <Route exact path="/admin" component={() => { window.location.href = '/admin/users'}} />
        <Route exact path="/admin/users" component={AdminUsers} />
        <Route exact path="/admin/classes/body_mind" component={AdminClassBM} />
        <Route exact path="/admin/classes/cardio_fitness" component={AdminClassC} />
        <Route exact path="/admin/users/expired" component={ExpiredUsers} />
        <Route exact path="/admin/users/bodymind" component={BodyMindUsers} />
        <Route exact path="/admin/users/cardio" component={CardioUsers} />
        <Route exact path="/admin/users/cardiobodymind" component={CardioBodyMindUsers} />

        {/* 404 */}
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  ); 
}
