import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { API } from '../../containers/App/constants';
import Cookies from '../../utils/cookies';

import RecordFactory from '../../utils/factory';

let factory = new RecordFactory();

// Styles 

import './ClassCard.scss';

let token = Cookies.getToken('accesstoken');
export default class ClassCard extends React.PureComponent {
  render() {
    var av_seats, classdate, unenroll;

    if (this.props.seats) {
      av_seats = <span className="seats">{this.props.seats} posti restanti</span>;
    }

    if (this.props.classdate) {
      classdate = <p className="day">{this.props.classdate}</p>;
    }

    if (this.props.unenroll == 'yes') {
      unenroll = <div style={{marginBottom: '25px', textAlign: 'center'}}>
                  <button className="enroll_button" onClick={this.handleClick} cid={this.props.cid} sid={this.props.session} cdate={this.props.classdate} caution={this.props.caution}>Rimuovi la mia iscrizione!</button>
                </div>;
    }
    
    return(
      <div>
        <article className={this.props.classtype + ' classCard'}>
          <span className="overlay"></span>
          <a href={'/classes/' + this.props.guid + '/' + this.props.session + '/?date=' + this.props.date} className="overlayLink"></a>
          <div className="main">
              <span className="title">{this.props.title}</span>
              {classdate}
              <p className="hours">
                  Ore: dalle <span>{this.props.from}</span> alle <span>{this.props.to}</span>
              </p>
          </div>
          {
            this.props.trainer && (
              <div className="side">
                  <span className="trainer">Trainer: {this.props.trainer} </span>
                  {/* {av_seats} */}
              </div>
            )
          }
        </article>
        {unenroll}
      </div>
    )
  }

  handleClick(e) {
    let cid = e.target.getAttribute('cid');
    let sid = e.target.getAttribute('sid');
    let cdate = e.target.getAttribute('cdate');
    let caution = e.target.getAttribute('caution');

    factory.unEnrollUserToClass(API, token, cid, sid, cdate, caution, Cookies);
  }
}
