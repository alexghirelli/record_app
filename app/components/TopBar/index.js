import React from 'react';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RecordFactory from '../../utils/factory';

let factory = new RecordFactory();

// Font awesome
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Styles 

import './TopBar.scss';

var todayDate = null;
var todayMaxDate = null;
var urlParams = new URLSearchParams(window.location.search);
var pDate = urlParams.get('date');
var currentSession = urlParams.get('session');

if (pDate) {
  todayDate = new Date(pDate);
  todayMaxDate = new Date(pDate);
} else {
  todayDate = new Date();
  todayMaxDate = new Date();
}

var minToday = new Date();
var maxToday = todayMaxDate.setDate(todayMaxDate.getDate()+14);

export default class StatusBar extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      targetDate: factory.formatDate(todayDate)
    };
  }

  goToOtherDay(date) {
    let daynumber = new Date(date).getDay();

    if (this.props.admin == 'true') {
      document.location = `/admin/classes/${this.props.trigger}/?date=${date}`;
    } else {
      document.location = `/classes/next/${daynumber}?date=${date}`;
    }
  }

  render() {

    var topbarContent, dateSelect, userData;

    if (this.props.calendar == 'true') {
      userData = <div className={'rl'}>
        <span>
          Lezioni restanti: <b>{this.props.rl}</b>
        </span>
        <span>
          Da poter prenotare: <b>{this.props.bl}</b>
        </span>
        <span>
          Iscrizione valida fino a: <b>{new Date(moment(this.props.su).toDate()).toLocaleString()}</b>
        </span>
      </div>

      dateSelect = <div>
        {!this.props.namespace && userData}
        <div className={'selectorDate'}>
          <p>Seleziona una data</p>
          <input type="date" value={this.state.targetDate} min={factory.formatDate(minToday)} max={factory.formatDate(maxToday)} onChange={(event) => { this.setState({targetDate: event.target.value}); event.preventDefault(); this.goToOtherDay(event.target.value);}} />
        </div>
      </div>;
    }

    if (this.props.admin == 'true') {
      topbarContent = <div className="topBar admin">
          <h4>{this.props.children}</h4>
          <ul>
            <li>
              <a href={'/admin/classes/body_mind'}>Allenamenti BodyMind</a>
            </li>
            <li>
              <a href={'/admin/classes/cardio_fitness'}>Allenamenti Palestra Pesi e Cardio</a>
            </li>
            <li>
              <a href={'/admin/users'}>Utenti</a>
            </li>
          </ul>

        { this.props.namespace === 'users' && (
          <ul className="secondlevel">
            <li>
              <a href={'/admin/users'}>Tutti gli utenti</a>
            </li>
            <li>
              <a href={'/admin/users/bodymind'}>Body Mind</a>
            </li>
            <li>
              <a href={'/admin/users/cardio'}>Cardio Fitness</a>
            </li>
            <li>
              <a href={'/admin/users/cardiobodymind'}>Cardio Fitness + Body Mind</a>
            </li>
            <li>
              <a href={'/admin/users/expired'}>Utenze scadute</a>
            </li>
          </ul>
        )}

        {dateSelect}
        { this.props.namespace === 'classes' && (
          <ul className="secondlevel hours">
            {this.props.classes.map((classEl, i) => {     
              return (<li><a href={`/admin/classes/${classEl.category}/?date=${this.state.targetDate}&session=${classEl.session}`} aria-selected={currentSession == classEl.session}>{classEl.from} - {classEl.to}</a></li>) 
            })}
            { this.props.classes.forEach(classEl => (<li>ciao</li>)) }
          </ul>
        )}
      </div>
    } else {
      topbarContent = <div className="topBar">
        {dateSelect}
      </div>
    }

    if (!this.props.calendar && !this.props.admin) {
      topbarContent = <div className="topBar">
        <p style={{float: 'none'}}>{this.props.title}</p>
      </div>
    }

    return(
      <div>
        {topbarContent}
      </div>      
    )
  }
}
