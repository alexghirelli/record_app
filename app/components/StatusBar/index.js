import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import Cookies from '../../utils/cookies';

// Font awesome
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Styles 

import './StatusBar.scss';

export default class StatusBar extends React.PureComponent {
  render() {
    return(
      <div className="statusbar">
        <ul>
          <li>
            <a href="/classes/next/">Home</a>
          </li>

          <li>
            <a href="/classes/booked">Classi prenotate</a>
          </li>

          {/* <li>
            <a href="user">Utente</a>
          </li> */}

          <li>
            <a href="#" onClick={this.logout}>Esci</a>
          </li>
        </ul>
      </div>      
    )
  }

  logout() {
    Cookies.eraseCookie('accesstoken');
    document.location.href = '/';
  }
}
