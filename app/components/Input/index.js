import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

// Styles 

import './Input.scss';

export default class RecordInput extends React.PureComponent {
  render() {
    return(
      <input className={'input_default'} type={this.props.type} placeholder={this.props.placeholder.props.defaultMessage} name={this.props.name} value={this.props.message} onChange={this.props.onChange} required={this.props.required == 'true' ? 'true' : 'false' }></input>
    )
  }
}
