import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

// Styles 

import './Button.scss';

export default class RecordButton extends React.PureComponent {
  render() {
    if (this.props.type == 'register') {
      return (
        <a className={'btn_default ' + this.props.color} href="/register" id={this.props.id}>{this.props.message}</a>
      )
    } else {
      return(
        <button className={'btn_default ' + this.props.color} id={this.props.id}>
          {this.props.message}
        </button>
      )
    }
  }
}
