export default class RecordFactory {
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
    
        return [year, month, day].join('-');
    }

    enrollUserToClass(api, token, cid, sid, cdate, caution, cookies) {
        var confirm = window.confirm('Sei sicuro di volerti registrare?');
    
        if (confirm) {
          fetch(api + 'classes/enroll', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify({
              classid: cid,
              sessionid: sid,
              classdate: cdate,
            })
          })
          .then((response) => response.json())
          .then((responseJson) => {

            if (!responseJson.enrolled && responseJson.message == 'account_freezed') {
              alert('Hai terminato il numero delle lezioni previste dal tuo abbonamento! Contatta la nostra segreteria per sapere come riattivarlo.');
              cookies.eraseCookie('accesstoken');
              document.location.href = '/';
            }
    
            if (responseJson.enrolled) {
              alert('Grazie per esserti registrato! Ci vediamo in palestra!');
              document.location.reload();
            }
    
          })
          .catch((error) => {
              console.log(error);
          });
        }
      }

    unEnrollUserToClass(api, token, cid, sid, cdate, caution, cookies) {
        var confirm = window.confirm('Sei sicuro di voler cancellare la tua prenotazione?');
    
        if (confirm) {
          fetch(api + 'classes/unenroll', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, */*',
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify({
              classid: cid,
              sessionid: sid,
              classdate: cdate,
              caution: caution
            })
          })
          .then((response) => response.json())
          .then((responseJson) => {
    
            if (!responseJson.enrolled && responseJson.message == 'user_unenrolled') {
              alert('La tua prenotazione è stata rimossa con successo e senza addebito!');
              document.location.reload();
            }

            if (!responseJson.enrolled && responseJson.message == 'lesson_scaled') {
              alert('Purtroppo sei arrivato tardi, la lezione verrà addebitata!');
              window.location.href = document.location.href += '&folded=1'
            }

            if (!responseJson.enrolled && responseJson.message == 'lesson_not_scaled') {
              alert('La tua prenotazione è stata rimossa, ricordati però che la prossima volta verrà addebitata!');
              document.location.reload();
            }

            if (!responseJson.enrolled && responseJson.message == 'account_freezed') {
              alert('Il tuo account è stato momentaneamente congelato, perché hai raggiunto il numero massimo di annullamenti non in tempo debito. Contatta la nostra segreteria per sapere come riattivarlo.');
              
              cookies.eraseCookie('accesstoken');
              document.location.href = '/';
            }
    
          })
          .catch((error) => {
              console.log(error);
          });
        }   
    }
}