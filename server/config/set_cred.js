// Set Salesforce credentials

var mysql_credentials = require('./mysql.json');

module.exports = {
    setMySQLCredentials: function(site) {
        var mysqlenv = mysql_credentials.env,
            _site = site;
    
        var host, 
            user, 
            password;
    
        switch(mysqlenv) {
            case 'production':
                host = mysql_credentials[_site].production.host,
                user = mysql_credentials[_site].production.users.root.username,
                password = mysql_credentials[_site].production.users.root.password;
            break;
    
            case 'sandbox':
                host = mysql_credentials[_site].sandbox.host,
                user = mysql_credentials[_site].sandbox.users.root.username,
                password = mysql_credentials[_site].sandbox.users.root.password;
            break;
    
            case 'local':
                host = mysql_credentials[_site].local.host,
                user = mysql_credentials[_site].local.users.root.username,
                password = mysql_credentials[_site].local.users.root.password;
            break;
        }
    
        var cred = {
            host: host,
            user: user,
            password: password
        };
    
        return cred;
    
    }
}