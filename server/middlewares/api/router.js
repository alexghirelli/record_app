const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const RecordApp = require('./api');
const AuthController = require('./auth');

// Init
const api = new RecordApp();
const auth = new AuthController();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

// middleware to use for all requests
router.use(function(req, res, next) {
    next();
});

// POST http://localhost:8080/api/auth/register
router.post('/auth/register', auth.register);

// POST http://localhost:8080/api/auth/login
router.post('/auth/login', auth.login);

// PUT http://localhost:8080/api/auth/remove/:uid
router.put('/auth/remove/:uid', auth.verifyToken, auth.removeUser);

// POST http://localhost:8080/api/auth/register
router.post('/auth/insertuser', auth.verifyToken, auth.insertUser);

// POST http://localhost:8080/api/classes/enroll
router.post('/classes/enroll', auth.verifyToken, api.enrollUserToClass);

// DELETE http://localhost:8080/api/classes/enroll
router.put('/classes/unenroll', auth.verifyToken, api.enrollUserToClass);

// DELETE http://localhost:8080/api/classes/admin/enroll
router.post('/admin/classes/user/remove', auth.verifyToken, api.removeUser);

// GET http://localhost:8080/api/classes/enrolled/:session/:date
router.get('/classes/enrolled/:session/:date', auth.verifyToken, api.isUserAlreadyEnrolled);

// PUT http://localhost:8080/api/user/enable
router.put('/user/enable/:uid', auth.verifyToken, api.enableUser);

// PUT http://localhost:8080/api/user/enable
router.put('/user/change/:uid', auth.verifyToken, api.changeUserData);

// GET http://localhost:8080/api/classes
router.get('/classes', auth.verifyToken, api.getClasses);

// GET http://localhost:8080/api/classes
router.get('/classes/booked', auth.verifyToken, api.getBookedClassesByUID);

// GET http://localhost:8080/api/classes/<ID_Class>
router.get('/classes/:guid', auth.verifyToken, api.getClassByGuid);

// GET http://localhost:8080/api/classes/date/:date
router.get('/classes/:ccategory/date/:date/:session', auth.verifyToken, api.getBookedClassesByDay);

// GET http://localhost:8080/api/classes/booked/<ID_Session>/date
router.get('/classes/booked/:sessionid/:date', auth.verifyToken, api.getBookedSlots);

// GET http://localhost:8080/api/classes/folded/<ID_Session>/date
router.get('/classes/folded/:sessionid/:date', auth.verifyToken, api.hasFolded);

// GET http://localhost:8080/api/users/
router.get('/users', auth.verifyToken, api.getUsers);

// GET http://localhost:8080/api/users/expired
router.get('/users/expired', auth.verifyToken, api.getExpiredUSers);

router.post('/users/alive', auth.verifyToken, api.aliveUser);

// GET http://localhost:8080/api/users/
router.get('/users/me', auth.verifyToken, api.getUserData);

module.exports = router;