var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../config/auth.json');

const mysql = require('mysql');

var mysql_credentials_obj = require('../../config/set_cred');
var mysql_credentials = mysql_credentials_obj.setMySQLCredentials('recordapp');

var db = mysql.createConnection({
  host: mysql_credentials.host,
  user: mysql_credentials.user,
  password: mysql_credentials.password,
  database: 'recordapp',
  dateStrings:true
});


module.exports = class AuthController {

    // Register new user
    async register (req, res) {
        var hashedPassword = bcrypt.hashSync(req.body.password, 8);

        db.query(
            `
            SELECT id from app_users WHERE email = '`+ req.body.email +`';
            `, function (error, results, fields) {
            if (error) return res.status(500).send("There was a problem registering the user.")
            // if (error) return res.status(500).send(error)
            if (results.length == 0) {
                db.query(
                    `
                    INSERT INTO app_users (id, name, surname, email, phone, password, banned, freezed, subscription, subscription_type, counter, isactive, permission)
                    VALUES (null,"`+ req.body.nome +`","`+ req.body.cognome +`",'`+ req.body.email +`','`+ req.body.phone +`','`+ hashedPassword +`',0, 0,'`+ req.body.subscription +`','`+ req.body.subscription_type +`',0, 0, 'basic');
                    `, function (error, results, fields) {
                    if (error) return res.status(500).send("There was a problem registering the user.")
                    var token = jwt.sign(
                        { 
                            id: results.insertId,
                            permission: 'basic'
                        }, 
                        config.secret, 
                        {
                            expiresIn: 2592000 // expires in 24 hours 
                        }
                    );
                    res.status(200).send(
                        { 
                            auth: true, 
                            token: token,
                            permission: 'basic',
                            expires: 2592000,
                        }
                    );
                });
            } else {
                res.status(403).send({
                    auth: false,
                    message: 'already_registered'
                });
            }
        });
    }

    async insertUser(req, res) {
        var hashedPassword = bcrypt.hashSync(req.body.password, 8);
        var subscribeduntil = '';
        var remaining_lessons = 0;

        if (req.body.subscribeduntil) {
            subscribeduntil = new Date(req.body.subscribeduntil).toISOString().slice(0, 19).replace('T', ' ')
        }

        if (req.body.remaining_lessons) {
            remaining_lessons = req.body.remaining_lessons;
        }

        if (req.perms == 'admin') {
            db.query(
                `
                SELECT id from app_users WHERE email = '`+ req.body.email +`';
                `, function (error, results, fields) {
                if (error) return res.status(500).send("There was a problem registering the user.")
                // if (error) return res.status(500).send(error)
                if (results.length == 0) {
                    db.query(
                        `
                        INSERT INTO app_users (id, name, surname, email, phone, password, banned, freezed, subscription, subscription_type, counter, isactive, permission, remaining_lessons, subscribed_until)
                        VALUES (null,"`+ req.body.nome +`","`+ req.body.cognome +`",'`+ req.body.email +`','`+ req.body.phone +`','`+ hashedPassword +`',0, 0,'`+ req.body.subscription +`','`+ req.body.subscription_type +`',0, ` + req.body.userstatus + `, 'basic', ` + remaining_lessons + `, '` + subscribeduntil + `');
                        `, function (error, results, fields) {
                        if (error) return res.status(500).send("There was a problem registering the user.")
                        var token = jwt.sign(
                            { 
                                id: results.insertId,
                                permission: 'basic'
                            }, 
                            config.secret, 
                            {
                                expiresIn: 2592000 // expires in 24 hours 
                            }
                        );
                        res.status(200).send(
                            { 
                                auth: true, 
                                token: token,
                                permission: 'basic',
                                expires: 2592000,
                                message: 'user_registered'
                            }
                        );
                    });
                } else {
                    res.status(403).send({
                        auth: false,
                        message: 'already_registered'
                    });
                }
            });
        } else {
            res.status(401).send({
                status: false,
                message: 'not_allowed'
            });
        }
    }

    async removeUser(req, res) {
        if (req.perms == 'admin') {
            db.query(
                `
                DELETE FROM app_users
                WHERE id = ` + req.params.uid + `
                `, function (error, results, fields) {
                if (error) return res.status(500).send("There was a problem registering the user.")
                
                res.status(200).send({
                    deleted: true,
                    message: 'user_removed'
                })
            });
        } else {
            res.status(401).send({
                message: 'not_allowed'
            })
        }
    }

    // Login user
    async login (req, res) {
        db.query(
            `
            SELECT id, email, password, permission, freezed, remaining_lessons, isactive, subscribed_until FROM app_users WHERE email = '`+ req.body.email +`';
            `, function(error, results, fields) {
            if (error) return res.status(500).send({
                auth: false,
                message: 'impossible_find_user'
            })

            if (results.length == 0) return res.status(404).send({
                auth: false,
                message: 'user_not_found'
            });
            
            var passwordIsValid = bcrypt.compareSync(req.body.password, results[0].password);
            if (!passwordIsValid) return res.status(401).send(
                { 
                    auth: false, 
                    message: 'password_not_valid'
                }
            );

            

            if (results[0].isactive == 1) {
                // if (results[0].freezed == 1 && results[0].remaining_lessons == 0) {
                //     res.status(200).send(
                //         {
                //             auth:false,
                //             message: "Il tuo account è stato momentaneamente congelato, perché hai terminato il numero di lezioni. Contatta la nostra segreteria per sapere come riattivarlo."
                //         }
                //     );
                // }

                if (results[0].freezed == 1) {
                    res.status(200).send(
                        {
                            auth:false,
                            message: "Il tuo account è stato momentaneamente congelato, perché hai raggiunto il numero massimo di annullamenti. Contatta la nostra segreteria per sapere come riattivarlo."
                        }
                    )
                } else {
                    if (new Date(results[0].subscribed_until).getTime() < new Date().getTime()) {
                        res.status(200).send(
                            {
                                auth:false,
                                message: "Il tuo abbonamento è scaduto. E' sufficiente rinnovare l'iscrizione annuale per riattivarlo."
                            }
                        );
                    } else {
                        var token = jwt.sign(
                            { 
                                id: results[0].id, 
                                permission: results[0].permission
                            }, 
                            config.secret, {
                                expiresIn: 2592000
                            }
                        );
            
                        res.status(200).send(
                            { 
                                auth: true, 
                                token: token, 
                                permission: results[0].permission, 
                                expires: 2592000 
                            }
                        );   
                    }

                    // var token = jwt.sign(
                    //     { 
                    //         id: results[0].id, 
                    //         permission: results[0].permission
                    //     }, 
                    //     config.secret, {
                    //         expiresIn: 2592000
                    //     }
                    // );
        
                    // res.status(200).send(
                    //     { 
                    //         auth: true, 
                    //         token: token, 
                    //         permission: results[0].permission, 
                    //         expires: 2592000 
                    //     }
                    // ); 
                }
            } else {
                res.status(200).send(
                    {
                        auth:false,
                        message: "Il tuo account è ancora in fase di approvazione! Riprova più tardi."
                    }
                )
            }
            
        });
    }

    // Verify permission for API call
    async verifyToken(req, res, next){
        var token = req.headers['x-access-token'];
        if (!token)
            return res.status(403).send(
                { 
                    auth: false, 
                    message: 'no_token_provided' 
                }
            );
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err)
            return res.status(500).send(
                { 
                    auth: false, 
                    message: 'token_auth_fail' 
                }
            );
            // if everything good, save to request for use in other routes
            req.userId = decoded.id;
            req.perms = decoded.permission;

            next();
        });
    }
}