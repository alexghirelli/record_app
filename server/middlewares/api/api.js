const mysql = require('mysql');

var mysql_credentials_obj = require('../../config/set_cred');
var mysql_credentials = mysql_credentials_obj.setMySQLCredentials('recordapp');

var db = mysql.createConnection({
  host: mysql_credentials.host,
  user: mysql_credentials.user,
  password: mysql_credentials.password,
  database: 'recordapp',
  dateStrings:true
});

module.exports = class RecordApp {

    // Classes
    async getClasses(req, res) {
        let day = req.query.d;
        let uid = req.query.uid;
        let cat = req.query.cat;

        if (day && !cat) {
            // Get available classes by day
            db.query(
                `
                SELECT c.id, c.title, c.description, c.category, c.isactive, s.guid, s.id as session, s.trainer, c.calories, c.duration, c.type, s.from, s.to, s.av_slots
                FROM app_classes c
                JOIN app_sessions s ON s.class = c.id AND s.day = `+ day +`
                WHERE c.isactive = 1
                ORDER BY s.from ASC, c.title;
                `, function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            });
        } else if (day && cat) {
            db.query(
                `
                SELECT c.id, c.title, c.description, c.category, c.isactive, s.guid, s.id as session, s.trainer, c.calories, c.duration, c.type, s.from, s.to, s.av_slots
                FROM app_classes c
                JOIN app_sessions s ON s.class = c.id AND s.day = `+ day +`
                WHERE c.isactive = 1 AND c.category = '`+ cat +`'
                ORDER BY s.from ASC, c.title;
                `, function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            });
        } else if (uid) {
            // Get booked classes by user
            db.query(
                `
                SELECT DISTINCT (c.title), c.description, c.calories, c.duration, c.type, s.from, s.to, e.enrollment_date
                FROM app_classes c
                JOIN app_sessions s ON s.class = c.id
                JOIN app_enrollment e ON e.session = s.id
                WHERE e.user = ` + uid + ` AND DATE(e.class_date) >= DATE(NOW());
                `, function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            });
        } else {
            // Get all available classes 
            db.query(
                `
                SELECT c.title, c.description, c.calories, c.duration
                FROM app_classes c;
                `, function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            });
        }
    }

    // Get class by id
    async getClassByGuid(req, res) {
        let guid = req.params.guid;

        db.query(
            `
            SELECT DISTINCT (s.guid), s.id as session, c.id as class, c.title, c.description, c.type, c.calories, c.duration, s.trainer, s.av_slots, s.from, s.to, s.caution_time
            FROM app_classes c
            JOIN app_sessions s
            WHERE s.guid = '` + guid + `' AND s.class = c.id;
            `, function (error, results, fields) {
            if (error) throw error;
            res.send(results);
        });
    }

    // Get booked class number

    async getBookedSlots(req, res) {
        let session = req.params.sessionid;
        let date = req.params.date;

        db.query(
            `
            SELECT 
                count(id) as slots_booked,
                ((SELECT aps.av_slots FROM app_sessions aps WHERE aps.id = ${session}) - count(id)) as available
            FROM app_enrollment ae
            WHERE (ae.session = `+ session +` AND ae.class_date = '`+ date +`' AND ae.folded = 0)
            `, function (error, results, fields) {
            if (error) throw error;
            res.send(results[0]);
        });
    }

    // Get booked classes by uid
    
    async getBookedClassesByUID(req, res) {
        let uid = req.userId;

        db.query(
            `
            SELECT c.id as cid, c.title, c.description, c.type, c.calories, c.duration, s.from, s.to, s.trainer, e.class_date, s.caution_time, s.guid, s.id as session
            FROM app_classes c
            JOIN app_enrollment e ON e.class = c.id
            JOIN app_sessions s ON s.id = e.session
            WHERE e.user = ` + uid + ` AND CONCAT(e.class_date,' ',s.to) >= NOW() AND e.folded = 0
            ORDER BY e.class_date ASC, c.title, s.from;
            `, function (error, results, fields) {
            if (error) throw error;
            res.send(results);
        });
    }

    async getBookedClassesByDay(req, res) {
        let date = req.params.date;
        let session = req.params.session;
        let classCategory = req.params.ccategory;

        db.query(
            `
            SELECT c.id as classid, s.id as session, s.caution_time, c.title, c.type, s.from, s.to, u.id as uid, u.name, u.surname, u.email, u.phone, e.enrollment_date, e.class_date
            FROM app_users u
            JOIN app_enrollment e ON e.user = u.id
            JOIN app_sessions s ON s.id = e.session
            JOIN app_classes c ON c.id = e.class
            WHERE e.class_date = '` + date + `' AND s.id = '` + session + `' AND c.category = '` + classCategory + `' AND e.folded = 0
            ORDER BY s.from ASC, c.title, e.enrollment_date;
            `, function (error, results, fields) {
            if (error) throw error;
            res.send(results);
        });
    }

    // Reset class presence counter
    async autoScaleClassFromUsere(req, res) {
        var dateNow = new Date();

        db.query(
            `
            SELECT u.id as user, c.id as cid, s.from, s.to, e.class_date, s.id as session
            FROM app_classes c
            JOIN app_enrollment e ON e.class = c.id
            JOIN app_sessions s ON s.id = e.session
            JOIN app_users u ON u.id = e.user;
            `, function (error, results, fields) {
            if (error) throw error;
            //res.send(results);

            var startTime = results.class_date + results.from;
            
            res.send(startTime);
        });
    }

    /*********************** User ***********************/

    // Enable User
    async enableUser(req, res) {
        let uid = req.params.uid;

        if (req.perms == 'admin') {
            db.query(
                `
                UPDATE app_users
                SET isactive = 1
                WHERE id = `+ uid +`
                `, function (error, results, fields) {
                if (error) throw error;
                res.status(200).send({
                    message: 'user_enabled'
                })
            });
        } else {
            res.status(401).send({
                message: 'you_are_not_allowed'
            });
        }
    }

    async changeUserData(req,res) {
        let uid = req.body.uid;
        let email = req.body.email;
        let phone = req.body.phone;
        let freezed = req.body.freezed;
        let subscription = req.body.subscription;
        let subscriptionType = req.body.subscription_type;
        let isactive = req.body.isactive;
        let new_lessons = 0;
        let subscribed_until = req.body.subscribeduntil;
        let remaining_lessons;

        if (req.body.new_lessons) {
            new_lessons = req.body.new_lessons;
        }

        let counter = req.body.counter;

        if (req.perms == 'admin') {
            db.query(
                `
                SELECT u.subscription
                FROM app_users as u
                WHERE id = `+ uid +`;
                `, function (error, results, fields) {
                if (error) throw error;
                
                if (results[0].subscription == 'open' && results[0].subscription != subscription) {
                    remaining_lessons = 0;

                    db.query(
                        `
                        UPDATE app_users SET
                            email = '`+ email +`',
                            phone = '`+ phone +`',
                            freezed = `+ freezed +`,
                            subscription = '`+ subscription +`',
                            subscription_type = '`+ subscriptionType +`',
                            isactive = `+ isactive +`,
                            remaining_lessons = ${remaining_lessons} + ${new_lessons},
                            counter = ` + counter + `,
                            subscribed_until = '` + subscribed_until + `'
                        WHERE id = `+ uid +`;
                        `, function (error, results, fields) {
                        if (error) throw error;
                        res.status(200).send({
                            message: 'subscription_changed'
                        })
                    });
                } else {
                    db.query(
                        `
                        UPDATE app_users SET
                            email = '`+ email +`',
                            phone = '`+ phone +`',
                            freezed = `+ freezed +`,
                            subscription = '`+ subscription +`',
                            subscription_type = '`+ subscriptionType +`',
                            isactive = `+ isactive +`,
                            remaining_lessons = remaining_lessons + `+ new_lessons +`,
                            counter = ` + counter + `,
                            subscribed_until = '` + subscribed_until + `'
                        WHERE id = `+ uid +`;
                        `, function (error, results, fields) {
                        if (error) throw error;
                        res.status(200).send({
                            message: 'subscription_changed'
                        })
                    });
                }
            });
        } else {
            res.status(401).send({
                message: 'you_are_not_allowed'
            });
        }
    }

    // Enroll user to a class

    async isUserAlreadyEnrolled(req,res) {
        let sessionid = req.params.session;
        let uid = req.userId;
        let class_date = req.params.date;

        db.query(
            `
            SELECT count(id) as isAlreadyEnrolled
            FROM app_enrollment ae
            WHERE (ae.user = `+ uid +` AND ae.session = `+ sessionid +` AND ae.class_date = '`+ class_date +`');
            `, function (error, results, fields) {
            if (error) throw error;

            if (results[0].isAlreadyEnrolled > 0) {
                res.status(200).send({
                    alreadyEnrolled: true
                })
            } else {
                res.status(200).send({
                    alreadyEnrolled: false
                })
            }
        });
    }

    async removeUser(req, res) {      
        let classId = req.body.classid;
        let sessionid = req.body.sessionid;
        let class_date = req.body.classdate;
        let uid = req.body.userid;


        if (req.perms == 'admin') {
            console.log(`
            DELETE 
            FROM app_enrollment
            WHERE class = '` + classId + `' AND session = '` + sessionid + `' AND user = '` + uid + `' AND class_date = '` + class_date + `';
            `);
            db.query(
                `
                DELETE 
                FROM app_enrollment
                WHERE class = '` + classId + `' AND session = '` + sessionid + `' AND user = '` + uid + `' AND class_date = '` + class_date + `';
                `, function (error, results, fields) {
                    console.log(error, results, fields)
                if (error) throw error;
    
                db.query(
                    `
                    UPDATE app_users
                    SET remaining_lessons = remaining_lessons + 1
                    WHERE id = ` + uid + `;
                    `, function (error, results, fields) {
                    if (error) throw error;
                    
                    res.status(200).send({
                        enrolled: false,
                        message: 'user_unenrolled'
                    })
                });
            });
        } else {
            res.status(401).send({
                message: 'you_are_not_allowed'
            });
        }
    }

    async enrollUserToClass(req, res) {
        let classId = req.body.classid;
        let sessionid = req.body.sessionid;
        let uid = req.userId;

        if (req.body.userid) {
            uid = req.body.userid;
        }

        let class_date = req.body.classdate;
        let enroll_date = new Date().toISOString().slice(0, 19).replace('T', ' ');

        switch(req.method) {
            case 'POST':

            db.query(
                `
                SELECT remaining_lessons
                FROM app_users u
                WHERE u.id = ` + uid + `;
                `, function (error, results, fields) {
                if (error) throw error;

                if (results[0].remaining_lessons == 0) {
                    db.query(
                        `
                        UPDATE app_users u
                        SET freezed = 1
                        WHERE u.id = ` + uid + `;
                        `, function (error, results, fields) {
                        if (error) throw error;
        
                        res.status(200).send({
                            enrolled: false,
                            message: 'account_freezed'
                        })
                    });
                } else {
                    db.query(
                        `
                        INSERT INTO app_enrollment (id, user, session, class, partecipated, class_date, enrollment_date)
                        VALUES (null,` + uid + `,` + sessionid + `,` + classId + `,0,'` + class_date + `','` + enroll_date + `');
                        `, function (error, results, fields) {
                        if (error) throw error;
        
                        db.query(
                            `
                            UPDATE app_users
                            SET remaining_lessons = remaining_lessons - 1
                            WHERE id = ` + uid + `;
                            `, function (error, results, fields) {
                            if (error) throw error;
                            
                            res.status(200).send({
                                enrolled: true,
                                message: 'user_enrolled'
                            })
                        });
                    });
                }
            });

            break;

            case 'PUT':
            let from = req.body.caution;
            let unenrollAgreeDate = new Date(class_date + ' ' +  from);
            unenrollAgreeDate.setHours(unenrollAgreeDate.getHours() + 1);

            let nowDate = new Date();
            nowDate.setHours(nowDate.getHours() + 1);

            db.query(
                `
                SELECT counter
                FROM app_users u
                WHERE u.id = ` + uid + `;
                `, function (error, results, fields) {
                if (error) throw error;

                if (results[0].counter >= 3) {
                    db.query(
                        `
                        UPDATE app_users u
                        SET freezed = 1
                        WHERE u.id = ` + uid + `;
                        `, function (error, results, fields) {
                        if (error) throw error;
        
                        res.status(200).send({
                            enrolled: false,
                            message: 'account_freezed'
                        })
                    });
                } else {
                    if (nowDate < unenrollAgreeDate) {
                        db.query(
                            `
                            DELETE 
                            FROM app_enrollment
                            WHERE class = '` + classId + `' AND session = '` + sessionid + `' AND user = '` + uid + `' AND class_date = '` + class_date + `';
                            `, function (error, results, fields) {
                            if (error) throw error;

                            db.query(
                                `
                                UPDATE app_users
                                SET remaining_lessons = remaining_lessons + 1
                                WHERE id = ` + uid + `;
                                `, function (error, results, fields) {
                                if (error) throw error;
                                
                                res.status(200).send({
                                    enrolled: false,
                                    message: 'user_unenrolled'
                                })
                            });
                        });
                    } else {
                        db.query(
                            `
                            SELECT isfirst_fold
                            FROM app_users u
                            WHERE u.id = ` + uid + `
                            `, function (error, results, fields) {
                            if (error) throw error;
        
                            if (results[0].isfirst_fold == 0) {
                                db.query(
                                    `
                                    UPDATE app_users SET
                                    isfirst_fold = 1,
                                    remaining_lessons = remaining_lessons + 1
                                    WHERE id = ` + uid + `;
                                    `, function (error, results, fields) {
                                    if (error) throw error;

                                    db.query(
                                        `
                                        DELETE 
                                        FROM app_enrollment
                                        WHERE class = '` + classId + `' AND session = '` + sessionid + `' AND user = '` + uid + `' AND class_date = '` + class_date + `';
                                        `, function (error, results, fields) {
                                        if (error) throw error;
                        
                                        db.query(
                                            `
                                            UPDATE app_enrollment
                                            SET folded = 1
                                            WHERE user = ` + uid + ` AND session = ` + sessionid + `;
                                            `, function (error, results, fields) {
                                            if (error) throw error;
                            
                                            res.status(200).send({
                                                enrolled: false,
                                                message: 'lesson_not_scaled'
                                            })
                                        });
                                    });
                                });
                            } else {
                                db.query(
                                    `
                                    UPDATE app_enrollment
                                    SET folded = 1
                                    WHERE user = ` + uid + ` AND session = ` + sessionid + `;
                                    `, function (error, results, fields) {
                                    if (error) throw error;
                    
                                    db.query(
                                        `
                                        UPDATE app_users
                                        SET counter = counter + 1
                                        WHERE id = ` + uid + `;
                                        `, function (error, results, fields) {
                                        if (error) throw error;
                        
                                        res.status(200).send({
                                            enrolled: false,
                                            message: 'lesson_scaled'
                                        })
                                    });
                                });
                            }
                        });
                    }
                }
            });
            
            break;
        }
    }

    // GET ALL USERS
    async getUsers(req, res) {
        if (req.perms == 'admin' || req.perms == 'editor') {
            const subscription_type = req.query.type ? req.query.type.split(',') : '';
            // const type = `${subscription_type ? 'AND subscription_type ='}`
            db.query(
                `
                SELECT u.id, u.name, u.surname, u.email, u.phone, u.banned, u.freezed, u.subscription, u.isactive, u.subscribed_until, remaining_lessons, u.counter as fold,
                (SELECT COUNT(e.id) FROM app_enrollment e WHERE e.user = u.id AND DATE(e.class_date) >= DATE(NOW())) as booked_lessons,
                (SELECT COUNT(e.id) FROM app_enrollment e WHERE e.user = u.id AND DATE(e.class_date) < DATE(NOW())) as done_lessons
                FROM app_users u
                WHERE (DATE(u.subscribed_until) >= DATE(now()) || u.subscribed_until IS NULL) ${subscription_type[0] ? `AND subscription_type = '${subscription_type[0]}'` : '' } ${subscription_type[1] ? `OR subscription_type = '${subscription_type[1]}'` : '' }
                ORDER BY u.name ASC;
                `, function (error, results, fields) {
                if (error) throw error;

                res.status(200).send(results)
            });
        } else {
            res.status(401).send({
                status: false,
                message: 'Unable to load your request'
            });
        }
    }

    async getExpiredUSers(req, res) {
        if (req.perms == 'admin') {
            db.query(
                `
                SELECT u.id, u.name, u.surname, u.subscription, u.isactive, u.subscribed_until, u.remaining_lessons
                FROM app_users u
                WHERE DATE(u.subscribed_until) < DATE(now())
                ORDER BY u.name ASC;
                `, function (error, results, fields) {
                if (error) throw error;

                res.status(200).send(results)
            });
        } else {
            res.status(401).send({
                status: false,
                message: 'Unable to load your request'
            });
        }
    }

    async aliveUser(req, res) {
        if (req.perms == 'admin') {
            let subscription = req.body.subscription;
            let remaining_lessons = req.body.remaining_lessons;
            let subscribed_until = req.body.subscribed_until;
            let uid = req.body.uid;

            db.query(
                `
                UPDATE app_users 
                SET 
                    counter = 0, 
                    subscription = '${subscription}', 
                    remaining_lessons = remaining_lessons + ${remaining_lessons}, 
                    subscribed_until = '${subscribed_until}' 
                WHERE id = ${uid};
                `, function (error, results, fields) {
                if (error) throw error;

                res.status(200).send({
                    message: 'reenabled_subscription'
                })
            });
        } else {
            res.status(401).send({
                status: false,
                message: 'Unable to load your request'
            });
        }
    }

    async getUserData(req, res) {
        db.query(
            `
            SELECT u.id, u.name, u.surname, subscription_type, remaining_lessons, subscribed_until, permission,
            (SELECT COUNT(e.id) FROM app_enrollment e WHERE e.user = u.id AND DATE(e.class_date) >= DATE(NOW())) as booked_lessons,
            (SELECT COUNT(e.id) FROM app_enrollment e WHERE e.user = u.id AND DATE(e.class_date) < DATE(NOW())) as done_lessons
            FROM app_users u
            WHERE u.id = ` + req.userId + `
            ORDER BY u.name ASC;
            `, function (error, results, fields) {
            if (error) throw error;

            res.status(200).send(results)
        });
    }

    // HAS FOLDED

    async hasFolded(req,res) {
        db.query(
            `
            SELECT folded
            FROM app_enrollment e
            WHERE e.user = `+ req.userId +` AND e.session = `+ req.params.sessionid +` AND e.class_date = '`+ req.params.date +`';
            `, function (error, results, fields) {
            if (error) throw error;

            if (results.length == 0) {
                res.status(200).send({
                    folded: false
                })
            }

            if (results[0].folded == 1) {
                res.status(200).send({
                    folded: true
                })
            } else {
                res.status(200).send({
                    folded: false
                })
            }
        });
    }

}